//
//  CitiesViewController.swift
//  Restaurants Locator
//
//  Created by Roman Plotkin on 09/03/2019.
//  Copyright © 2019 Roman Plotkin. All rights reserved.
//

import UIKit
import GoogleMaps

class CitiesViewController: UIViewController {


    // MARK: - Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let camera = GMSCameraPosition.camera(withLatitude: 31.427606, longitude: 34.597094, zoom: 12.0)
        let mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        
        view = mapView
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: 31.427606, longitude: 34.597094)
        marker.title = "Netivot"
        marker.snippet = "My Home"
        marker.map = mapView
    }
}
